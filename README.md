# heapdump

Simple tool to dump the heap into a file from a Linux process.

## Building

```sh
make
```

You must have `glib-2.0` development headers available.

## Running

```sh
./heapdump PID
```

It will write files like `pid.heap.N` where pid is the process identifier, and N is a number `1..N`.

You might consider running strings on the output files to get a high-level overview of what is in the heap.
