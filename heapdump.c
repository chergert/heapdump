#include <errno.h>
#include <fcntl.h>
#include <glib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static void
copy_range (const gchar *mempath,
            const gchar *filename,
            guint64      begin,
            guint64      end)
{
  guint8 buf[16*1024];
  gsize to_read = end - begin;
  int in_fd, out_fd;

  g_assert (mempath != NULL);
  g_assert (filename != NULL);
  g_assert (end > begin);
  g_assert (to_read > 0);

  if (-1 == (in_fd = open (mempath, O_RDONLY)))
    {
      g_printerr ("Failed to open %s\n", mempath);
      exit (1);
    }

  if (-1 == (out_fd = open (filename, O_CREAT | O_WRONLY, 0750)))
    {
      g_printerr ("Failed to open %s\n", filename);
      exit (1);
    }

  if ((off_t)-1 == lseek (in_fd, begin, SEEK_SET))
    {
      g_printerr ("Failed to seek to 0x%"G_GINT64_MODIFIER"x\n", begin);
      exit (1);
    }

  while (to_read > 0)
    {
      gsize expected = to_read % sizeof buf;
      gssize len;

      if (expected == 0)
        expected = sizeof buf;

      errno = 0;
      len = read (in_fd, buf, expected);

      if (len < 0)
        {
          g_printerr ("Failed to read: %s\n", g_strerror (errno));
          exit (1);
        }

      if (len != expected)
        {
          g_printerr ("Failed to read %"G_GSIZE_FORMAT" bytes from %s\n",
                      expected, mempath);
          exit (1);
        }

      if (len != write (out_fd, buf, len))
        {
          g_printerr ("Failed to write %"G_GSIZE_FORMAT" bytes to %s\n",
                      len, filename);
          exit (1);
        }

      to_read -= len;
    }

  close (in_fd);
  close (out_fd);
}

int
main (int argc,
      char *argv[])
{
  if (argc == 1)
    {
      g_printerr ("usage: %s PID...\n", argv[0]);
      return EXIT_FAILURE;
    }

  for (guint i = 1; i < argc; i++)
    {
      g_autofree gchar *mempath = NULL;
      g_autofree gchar *path = NULL;
      g_autofree gchar *contents = NULL;
      g_autoptr(GError) error = NULL;
      g_auto(GStrv) lines = NULL;
      gint64 pid64;
      gsize len = 0;
      guint heap_count = 0;

      errno = 0;
      pid64 = g_ascii_strtoll (argv[i], NULL, 10);

      if (!g_ascii_isdigit (argv[i][0]) ||
          (pid64 == 0 && (errno != 0)) ||
          (pid64 < 0) ||
          (pid64 > G_MAXUSHORT))
        {
          g_printerr ("Invalid number provided: %s\n", argv[i]);
          return EXIT_FAILURE;
        }

      if (pid64 == 0)
        {
          g_printerr ("Cannot dump memory from pid 0\n");
          return EXIT_FAILURE;
        }

      g_assert (pid64 < G_MAXUSHORT);
      g_assert (pid64 > 0);

      path = g_strdup_printf ("/proc/%"G_GINT64_FORMAT"/maps", pid64);
      mempath = g_strdup_printf ("/proc/%"G_GINT64_FORMAT"/mem", pid64);

      if (!g_file_get_contents (path, &contents, &len, &error))
        {
          g_printerr ("Failed to access pid %s: %s\n", argv[i], error->message);
          return EXIT_FAILURE;
        }

      lines = g_strsplit (contents, "\n", 0);

      for (guint j = 0; lines[j]; j++)
        {
          if (strstr (lines[j], "[heap]") != NULL)
            {
              g_autofree gchar *heap_out = NULL;
              guint64 begin = 0;
              guint64 end = 0;

              if (2 != sscanf (lines[j], "%"G_GINT64_MODIFIER"x-%"G_GINT64_MODIFIER"x ", &begin, &end))
                {
                  g_printerr ("Failed to parse line: %s\n", lines[j]);
                  return EXIT_FAILURE;
                }

              if (end <= begin)
                {
                  g_printerr ("Implausible memory range: %"G_GUINT64_FORMAT" - %"G_GUINT64_FORMAT"\n",
                              begin, end);
                  return EXIT_FAILURE;
                }

              heap_count++;

              heap_out = g_strdup_printf ("%"G_GUINT64_FORMAT".heap.%u", pid64, heap_count);
              copy_range (mempath, heap_out, begin, end);
            }
        }
    }

  return EXIT_SUCCESS;
}
