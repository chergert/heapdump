all: heapdump

WARNINGS = -Wall
DEBUG = -g
PKGS = glib-2.0

heapdump: heapdump.c Makefile
	$(CC) -o $@.tmp $(WARNINGS) $(DEBUG) $(shell pkg-config --cflags --libs $(PKGS)) heapdump.c ; \
	mv $@.tmp $@

clean:
	rm -f heapdump
